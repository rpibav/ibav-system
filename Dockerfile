FROM  node:16.17.0

WORKDIR /usr/src/api

COPY . .

RUN npm install --quiet --no-optional --no-fund --loglevel=error

RUN npm ci
RUN npm run build
RUN npx prisma generate
RUN npx prisma migrate status
RUN npx prisma db push
RUN npx prisma migrate deploy

CMD ["npm", "run", "start:prod"]