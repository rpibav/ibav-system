/*
  Warnings:

  - The values [N] on the enum `Present` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "Present_new" AS ENUM ('F', 'P', 'D', 'V', 'T');
ALTER TABLE "attendances" ALTER COLUMN "cell" DROP DEFAULT;
ALTER TABLE "attendances" ALTER COLUMN "worship" DROP DEFAULT;
ALTER TABLE "attendances" ALTER COLUMN "worship" TYPE "Present_new" USING ("worship"::text::"Present_new");
ALTER TABLE "attendances" ALTER COLUMN "cell" TYPE "Present_new" USING ("cell"::text::"Present_new");
ALTER TYPE "Present" RENAME TO "Present_old";
ALTER TYPE "Present_new" RENAME TO "Present";
DROP TYPE "Present_old";
ALTER TABLE "attendances" ALTER COLUMN "cell" SET DEFAULT 'F';
ALTER TABLE "attendances" ALTER COLUMN "worship" SET DEFAULT 'F';
COMMIT;
