-- CreateEnum
CREATE TYPE "Present" AS ENUM ('F', 'N');

-- CreateTable
CREATE TABLE "reports" (
    "id" TEXT NOT NULL,
    "cell" TEXT NOT NULL,
    "observation" TEXT,
    "offer" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "date" TIMESTAMP(3) NOT NULL,
    "network" TEXT NOT NULL,
    "week" TEXT NOT NULL,
    "discipleship" TEXT NOT NULL,

    CONSTRAINT "reports_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "attendances" (
    "id" TEXT NOT NULL,
    "report_id" TEXT NOT NULL,
    "worship" "Present" NOT NULL DEFAULT 'F',
    "cell" "Present" NOT NULL DEFAULT 'F',
    "name" TEXT NOT NULL,
    "status" TEXT NOT NULL,

    CONSTRAINT "attendances_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "attendances" ADD CONSTRAINT "attendances_report_id_fkey" FOREIGN KEY ("report_id") REFERENCES "reports"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
