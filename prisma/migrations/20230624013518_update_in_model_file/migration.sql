/*
  Warnings:

  - You are about to drop the column `buffer` on the `File` table. All the data in the column will be lost.
  - You are about to drop the column `encoding` on the `File` table. All the data in the column will be lost.
  - You are about to drop the column `fieldname` on the `File` table. All the data in the column will be lost.
  - You are about to drop the column `mimetype` on the `File` table. All the data in the column will be lost.
  - You are about to drop the column `originalname` on the `File` table. All the data in the column will be lost.
  - You are about to drop the column `size` on the `File` table. All the data in the column will be lost.
  - Added the required column `base64` to the `File` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "File" DROP COLUMN "buffer",
DROP COLUMN "encoding",
DROP COLUMN "fieldname",
DROP COLUMN "mimetype",
DROP COLUMN "originalname",
DROP COLUMN "size",
ADD COLUMN     "base64" TEXT NOT NULL;
