export const getFirstWord = (wordWorship: string) => {
  const wordWithoutSpace = wordWorship.trim();
  const wordSeparator = wordWithoutSpace.split(' ');
  const firstWordFormatted = wordSeparator[0];

  return firstWordFormatted;
};
