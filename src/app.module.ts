import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { ReportModule, UploadModule } from './modules';

@Module({
  imports: [ConfigModule.forRoot(), ReportModule, UploadModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
