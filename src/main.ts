import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { env } from './env';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  const config = new DocumentBuilder()
    .setTitle('Documentação Endpoints - IBAV System')
    .setDescription(
      'Toda documentação dos endpoints cadastrados na aplicação irá ser contida na listagem abaixo, caso haja algum problema, deve ser reportado para o desenvolvedor responsável pelo o servidor',
    )
    .setVersion('1.0')
    .addTag('Reports')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(env.PORT);
}
bootstrap();
