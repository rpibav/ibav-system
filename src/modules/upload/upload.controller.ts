import { z } from 'zod';
import { FilesInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
  Get,
  Post,
  Param,
  Query,
  Delete,
  Controller,
  HttpStatus,
  UploadedFiles,
  HttpException,
  UseInterceptors,
} from '@nestjs/common';

import { CreateUploadDto } from './dto';
import { UploadService } from './upload.service';

@ApiTags('Upload')
@Controller('upload')
export class UploadController {
  constructor(private readonly uploadService: UploadService) {}

  @ApiOperation({ summary: 'Upload de arquivo' })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @Post()
  @UseInterceptors(FilesInterceptor('file'))
  async create(@UploadedFiles() files: Array<CreateUploadDto>) {
    const registerBodySchema = z.array(
      z.object({
        encoding: z.string(),
        mimetype: z.string(),
        fieldname: z.string(),
        originalname: z.string(),
        size: z.number(),
        buffer: z.any().refine((value) => value instanceof Buffer, {
          message: 'O campo buffer deve ser do tipo Buffer',
        }),
      }),
    );

    try {
      const validatedFiles = registerBodySchema.parse(files);

      for (const { buffer, originalname } of validatedFiles) {
        const base64 = buffer.toString('base64');
        await this.uploadService.uploadFile({
          base64,
          fileName: originalname,
        });
      }
    } catch (error) {
      if (error instanceof z.ZodError) {
        const errorMessage = error.errors.map((err) => err.message).join(', ');
        throw new HttpException(
          `Campos inválidos: ${errorMessage}`,
          HttpStatus.BAD_REQUEST,
        );
      }

      throw error;
    }
  }

  @Get()
  findAllUploads(@Query('typeWords') typeWords: string) {
    const formattedWordsInUppercase = typeWords?.toUpperCase();
    const arrayOfTypeWords = formattedWordsInUppercase?.split(',');

    return this.uploadService.findAll(arrayOfTypeWords);
  }

  @Get(':id')
  findUniqueUpload(@Param('id') id: string) {
    return this.uploadService.getById(id);
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return this.uploadService.removeById(id);
  }
}
