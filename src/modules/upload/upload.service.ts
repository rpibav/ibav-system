import { Prisma } from '@prisma/client';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';

import { PrismaService } from 'src/database/PrismaService';
import { getFirstWord } from 'src/helpers/getFirstWord';

@Injectable()
export class UploadService {
  constructor(private prisma: PrismaService) {}

  async uploadFile({ base64, fileName }: Prisma.FileCreateInput) {
    const firstWord = getFirstWord(fileName);

    const existingFiles = await this.prisma.file.findMany({
      where: {
        fileName: {
          contains: firstWord,
        },
      },
    });

    const filesWithFilenameEqual = existingFiles.length;

    if (filesWithFilenameEqual > 0) {
      const firstIdInFilesWithFilenameEqual = existingFiles[0].id;

      await this.prisma.file.update({
        where: {
          id: firstIdInFilesWithFilenameEqual,
        },
        data: {
          base64,
          fileName,
        },
      });
    } else {
      await this.prisma.file.create({
        data: {
          base64,
          fileName,
        },
      });
    }
  }

  async findAll(arrayOfTypeWords: string[]) {
    if (arrayOfTypeWords && arrayOfTypeWords.length > 0) {
      const response = await this.prisma.file.findMany({
        where: {
          OR: arrayOfTypeWords.map((word) => ({
            fileName: {
              contains: word,
            },
          })),
        },
        orderBy: {
          created_at: 'desc',
        },
      });

      return response;
    } else {
      const response = await this.prisma.file.findMany({
        orderBy: {
          created_at: 'desc',
        },
      });

      return response;
    }
  }

  async getById(id: string) {
    const cultWord = await this.prisma.file.findUnique({
      where: {
        id,
      },
    });

    if (!cultWord) {
      throw new HttpException(
        'Não existe palavra do culto com esse id',
        HttpStatus.NOT_FOUND,
      );
    }

    return cultWord;
  }

  async removeById(id: string) {
    const cultWord = await this.prisma.file.findUnique({
      where: {
        id,
      },
    });

    if (!cultWord) {
      throw new HttpException(
        'Não existe palavra do culto com esse id',
        HttpStatus.NOT_FOUND,
      );
    }

    const { fileName } = await this.prisma.file.delete({
      where: {
        id,
      },
    });

    return {
      message: `Arquivo: ${fileName} excluido com sucesso`,
    };
  }
}
