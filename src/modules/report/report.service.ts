import { Prisma } from '@prisma/client';
import { addDays, startOfDay } from 'date-fns';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';

import { PrismaService } from 'src/database/PrismaService';

import { CreateReportDto, QueryReportFormatted } from './dto';

@Injectable()
export class ReportService {
  constructor(private prisma: PrismaService) {}

  private buildFilters(
    date?: Date,
    network?: string,
    discipleship?: string,
    cell?: string,
  ): Prisma.ReportWhereInput {
    const filters: Prisma.ReportWhereInput = {};

    if (date) {
      const startDate = startOfDay(date);
      const nextDay = addDays(startDate, 1);

      filters.date = {
        gte: startDate,
        lt: nextDay,
      };
    }

    if (network) {
      filters.network = network;
    }

    if (discipleship) {
      filters.discipleship = discipleship;
    }

    if (cell) {
      filters.cell = cell;
    }

    return filters;
  }

  private async queryReports(
    page: number,
    pageSize: number,
    filters: Prisma.ReportWhereInput,
  ) {
    const reports = await this.prisma.report.findMany({
      skip: (page - 1) * pageSize,
      take: pageSize,
      where: filters,
      orderBy: {
        date: 'desc',
      },
      include: {
        attendance: true,
      },
    });

    return reports;
  }

  async create({
    week,
    cell,
    date,
    offer,
    network,
    attendance,
    observation,
    discipleship,
  }: CreateReportDto) {
    const currentDate = new Date();
    const dateRegistered = new Date(date);
    const isGreaterCurrentDate = dateRegistered > currentDate;

    if (isGreaterCurrentDate) {
      throw new HttpException(
        'Não é possível cadastrar datas futuras, apenas data atual ou retroativas',
        HttpStatus.CONFLICT,
      );
    }

    const report = await this.prisma.report.create({
      data: {
        cell,
        date,
        week,
        offer,
        network,
        discipleship,
        attendance: {
          create: attendance,
        },
        observation,
      },
    });

    return {
      id: report.id,
    };
  }

  async findAll({
    page,
    cell,
    date,
    network,
    pageSize,
    discipleship,
  }: QueryReportFormatted) {
    const totalCount = await this.prisma.report.count();
    const totalPages = Math.ceil(totalCount / pageSize);

    const filters = this.buildFilters(date, network, discipleship, cell);
    const reports = await this.queryReports(page, pageSize, filters);

    return {
      _metadata: {
        page,
        pageSize,
        totalCount,
        totalPages,
      },
      results: reports,
    };
  }

  async removeById(id: string) {
    const reportExists = await this.prisma.report.findUnique({
      where: {
        id,
      },
    });

    if (!reportExists) {
      throw new HttpException(
        'Não existe relatório com esse id',
        HttpStatus.NOT_FOUND,
      );
    }

    await this.prisma.attendance.deleteMany({
      where: {
        report_id: id,
      },
    });

    const data = await this.prisma.report.delete({
      where: {
        id,
      },
    });

    return {
      message: `Relatório ${data.id} excluido com sucesso`,
    };
  }

  async getById(id: string) {
    const report = await this.prisma.report.findUnique({
      where: {
        id,
      },
      include: {
        attendance: true,
      },
    });

    if (!report) {
      throw new HttpException(
        'Não existe relatório com esse id',
        HttpStatus.NOT_FOUND,
      );
    }

    return report;
  }
}
