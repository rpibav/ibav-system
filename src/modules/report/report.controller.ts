import { z } from 'zod';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import {
  Get,
  Body,
  Post,
  Param,
  Query,
  Delete,
  Controller,
  HttpStatus,
  HttpException,
} from '@nestjs/common';

import { ReportService } from './report.service';
import { AttendanceDto, CreateReportDto, ReportQueryDTO } from './dto';

@ApiTags('Reports')
@Controller('reports')
export class ReportController {
  constructor(private readonly reportService: ReportService) {}

  @Post()
  async create(@Body() body: CreateReportDto) {
    const attendanceSchema = z.object({
      name: z.string(),
      status: z.string(),
      cell: z.enum(['F', 'P', 'D', 'T', 'V']).default('F'),
      worship: z.enum(['F', 'P', 'D', 'T', 'V']).default('F'),
    });

    const registerBodySchema = z.object({
      date: z.string(),
      week: z.string(),
      cell: z.string(),
      offer: z.string(),
      network: z.string(),
      observation: z.string(),
      discipleship: z.string(),
      attendance: z.array(attendanceSchema),
    });

    try {
      const {
        cell,
        date,
        week,
        offer,
        network,
        attendance,
        observation,
        discipleship,
      } = registerBodySchema.parse(body);

      return this.reportService.create({
        cell,
        date,
        week,
        offer,
        network,
        discipleship,
        attendance: attendance as AttendanceDto[],
        observation: observation || 'Nenhuma observação',
      });
    } catch (error) {
      if (error instanceof z.ZodError) {
        const errorMessage = error.errors.map((err) => err.message).join(', ');
        throw new HttpException(
          `Campos inválidos: ${errorMessage}`,
          HttpStatus.BAD_REQUEST,
        );
      }

      throw error;
    }
  }

  @Get()
  @ApiParam({
    name: 'cell',
    type: 'string',
    description: 'Nome/Número da célula',
    required: false,
  })
  @ApiParam({
    name: 'network',
    type: 'string',
    description: 'Nome da rede',
    required: false,
  })
  @ApiParam({
    name: 'cell',
    type: 'string',
    description: 'Nome/Número da célula',
    required: false,
  })
  @ApiParam({
    name: 'cell',
    type: 'string',
    description: 'Nome/Número da célula',
    required: false,
  })
  @ApiParam({
    name: 'cell',
    type: 'string',
    description: 'Nome/Número da célula',
    required: false,
  })
  async findAll(@Query() query: ReportQueryDTO) {
    const cell = query.cell;
    const network = query.network;
    const page = parseInt(query.page) || 1;
    const discipleship = query.discipleship;
    const pageSize = parseInt(query.pageSize) || 10;
    const date = query.date ? new Date(query.date) : undefined;

    return this.reportService.findAll({
      page,
      date,
      cell,
      network,
      pageSize,
      discipleship,
    });
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return this.reportService.removeById(id);
  }

  @Get(':id')
  async getReportById(@Param('id') id: string) {
    return this.reportService.getById(id);
  }
}
