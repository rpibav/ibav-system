import { ApiProperty } from '@nestjs/swagger';

export class ReportQueryDTO {
  @ApiProperty({ description: 'O nome/número da célula' })
  cell?: string;

  @ApiProperty({ description: 'O nome da rede' })
  network?: string;

  @ApiProperty({ description: 'O número da página atual', default: 1 })
  page: string;

  @ApiProperty({ description: 'A quantidade de itens na página', default: 10 })
  pageSize: string;

  @ApiProperty({ description: 'O nome do discipulador' })
  discipleship?: string;

  @ApiProperty({
    description: 'A data do relatório da célula relacionada',
    type: 'string',
    format: 'date',
  })
  date?: Date;
}
