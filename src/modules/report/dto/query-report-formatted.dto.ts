export class QueryReportFormatted {
  date?: Date;
  page: number;
  cell?: string;
  pageSize: number;
  network?: string;
  discipleship?: string;
}
