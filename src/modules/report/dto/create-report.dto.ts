import { ApiProperty } from '@nestjs/swagger';

export class AttendanceDto {
  @ApiProperty({ description: 'O nome do membro', example: 'João' })
  name: string;

  @ApiProperty({
    description: 'O status do membro relacionado a célula',
    example: 'Frequentador assíduo',
  })
  status: string;

  @ApiProperty({
    description:
      'Se o membro foi ou não para célula, sendo F = Foi, N = Não foi',
    example: 'F',
  })
  cell: 'F' | 'P' | 'D' | 'V' | 'T';

  @ApiProperty({
    description:
      'Se o membro foi ou não para culto, sendo F = Foi, N = Não foi',
    example: 'F',
  })
  worship: 'F' | 'P' | 'D' | 'V' | 'T';
}

export class CreateReportDto {
  @ApiProperty({
    description: 'A data da célula relacionada ao relatório',
    example: '2023-06-13T23:59:45.000Z',
  })
  date: string;

  @ApiProperty({
    description: 'A semana da célula relacionado ao relatório',
    example: 'Primeira semana',
  })
  week: string;

  @ApiProperty({ description: 'O número/nome da célula', example: '01' })
  cell: string;

  @ApiProperty({
    description: 'O valor da oferta arrecadada',
    example: '50.44',
  })
  offer: string;

  @ApiProperty({ description: 'O nome da rede', example: 'Familia' })
  network: string;

  @ApiProperty({
    description: 'A relacionada a célula',
    example: 'Joãozinho comeu muito',
  })
  observation: string;

  @ApiProperty({ description: 'O nome do discipulador', example: 'Maria' })
  discipleship: string;

  @ApiProperty({
    description: 'A lista de presenças',
    type: [AttendanceDto],
  })
  attendance: AttendanceDto[];
}
